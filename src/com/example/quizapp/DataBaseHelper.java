package com.example.quizapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Class to handle local SQLite database 
 * @author it-hogskolan
 *
 */
public class DataBaseHelper extends SQLiteOpenHelper {

	private Context context;
	private SQLiteDatabase dataBase;
	private String category;
	private ArrayList<String> categories;

	/**
	 * Constructor
	 * @param context - Application context
	 */
	public DataBaseHelper(Context context) {
		super(context, "AllQuestions", null, 1);
		this.context = context;
		if (null == categories) {
			categories = new ArrayList<String>();
		}
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE Questions(" +
				"id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"category VARCHAR(100)," +
				"question VARCHAR(100)," +
				"option1 VARCHAR(100)," +
				"option2 VARCHAR(100)," +
				"option3 VARCHAR(100)," +
				"option4 VARCHAR(100)," +
				"option5 VARCHAR(100));");

		db.execSQL("CREATE TABLE Highscores(" +
				"id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"name VARCHAR(100)," +
				"score INTEGER);");

		dataBase = db;

		readCategories();
		ArrayList<String> allQuestions = readQuestionsFromTxt();
		for (int i = 0; i < allQuestions.size(); i++) {
			putQuestionInDatabase(db, allQuestions.get(i));
		}
	}

	/**
	 * Checks the assets-folder for existing .txt-files and reads them into ArrayList<String> 
	 * @return ArrayList<String> All questions in 'assets/questions/swedish'-folder
	 */
	public ArrayList<String> readQuestionsFromTxt() {

		ArrayList<String> allQuestions = new ArrayList<String>();

		try {

			AssetManager mngr = context.getAssets();
			String[] assets = mngr.list("questions/swedish");

			for (int i = 0; i < assets.length; i++) {
				BufferedReader br = new BufferedReader(new InputStreamReader(context.getAssets().open("questions/swedish/"+assets[i])));
				String line = categories.get(i);
				while (line != null) {
					allQuestions.add(line);
					line = br.readLine();
				}
				br.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return allQuestions;
	}

	/**
	 * Reads all existing filenames in 'assets/questions/swedish' and converts them into category headlines
	 */
	public void readCategories() {

		try {

			AssetManager mngr = context.getAssets();
			String[] assets = mngr.list("questions/swedish");

			for (int i = 0; i < assets.length; i++) {
				int endIndex = assets[i].indexOf(".");
				String category = assets[i].substring(0, endIndex).replace("_", " ");
				categories.add(category);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Takes a string, parses it as a question an adds it to SQlite database
	 * @param db SQLiteDatabse - The database to add the question to
	 * @param q String containing question, category and all options 
	 */
	public void putQuestionInDatabase(SQLiteDatabase db, String q) {

		ContentValues row = new ContentValues();

		String question = "";
		String[] options = new String[5];

		if (!q.contains("?")) {
			category = q;
			categories.add(q);
			return;
		} else {
			String[] s = q.split("#");
			question = s[0];
			for (int i = 0; i < options.length; i++) {
				options[i] = s[i+1];
			}
		}
		row.put("category", category);
		row.put("question", question);

		for (int i = 0; i < options.length; i++) {
			row.put("option"+(i+1), options[i]);
		}
		db.insert("Questions", null, row);
	}

	/**
	 * Reads a question from the database 
	 * @param questionId int Id of the question to read
	 * @return String[] containing id, question, options
	 */
	public String[] readQuestion(int questionId) {
		String[] strings = new String[8];
		dataBase = getReadableDatabase();
		Cursor cursor = dataBase.query("Questions", null, null, null, null, null, null, null);
		if (cursor.moveToPosition(questionId)) {
			for (int i = 0; i < strings.length; i++) {
				if (i == 0) {
					strings[i] = cursor.getPosition()+"";
				} else {
					strings[i] = cursor.getString(i);
				}
			}
		}
		return strings;
	}

	/**
	 * Counts rows in database
	 * @return int - number of rows in the database
	 */
	public int countRowsInDatabase() {
		dataBase = getReadableDatabase();
		Cursor cursor = dataBase.query("Questions", null, null, null, null, null, null, null);
		int rows = cursor.getCount();
		return rows;
	}

	/**
	 * Gets all questions belonging to the categories the user has picked
	 * @param userCategories - ArrayList<String> The user specified categories
	 * @return ArrayList<Integer> All ids of questions in picked categories
	 */
	public ArrayList<Integer> getIds(ArrayList<String> userCategories) {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		dataBase = getReadableDatabase();
		Cursor cursor = dataBase.query("Questions", null, null, null, null, null, null, null);
		if(cursor.moveToFirst()) {
			int index = 0;
			for (int i = 0; i < userCategories.size(); i++) {
				cursor.moveToFirst();
				do {
					if (cursor.getString(1).equals(userCategories.get(index))) {
						ids.add(cursor.getPosition());
					}
				} while (cursor.moveToNext());
				index++;
			}
		}
		return ids;
	}

	/**
	 * Help method, only used to print the entire database to LogCat
	 */
	public void printDB() {
		dataBase = getReadableDatabase();
		Cursor cursor = dataBase.query("Questions", null, null, null, null, null, null, null);
		String[] cn = cursor.getColumnNames();
		for (int i = 0; i<cn.length; i++) {
			Log.d("tagg", cn[i]);
		}
		cursor.moveToFirst();
		do {
			for (int i = 0; i < cn.length; i++) {
				Log.d("tagg", cursor.getString(i));
			}
		} while (cursor.moveToNext());
	}

	/**
	 * Adds a player score to the local highscores table
	 * @param name String - Name of player
	 * @param score int - Player score
	 */
	public void addHighscore(String name, int score) {
		dataBase = getWritableDatabase();
		ContentValues row = new ContentValues();
		row.put("name", name);
		row.put("score", score);
		dataBase.insert("Highscores", null, row);
	}

	/**
	 * Gets all highscores from the local highscores table
	 * @return ArrayList<Player> - All player scores and names
	 */
	public ArrayList<Player> getHighscores() {
		dataBase = getReadableDatabase();
		Cursor cursor = dataBase.query("Highscores", null, null, null, null, null, null, null);
		ArrayList<Player> players = new ArrayList<Player>();

		if(cursor.moveToFirst()) {

			do{
				Player player = new Player();
				player.setName(cursor.getString(1));
				player.setScore(cursor.getInt(2));
				players.add(player);
			}while(cursor.moveToNext());
		}
		return players;
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS Questions;");
		onCreate(db);
	}

	public ArrayList<String> getCategories() {
		readCategories();
		return categories;
	}
}
