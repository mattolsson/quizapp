package com.example.quizapp;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class GameActivity extends ActionBarActivity {

//	private MediaPlayer answer = new MediaPlayer();

	private GameMechanics gm;
	private ArrayList<Integer> ids = new ArrayList<Integer>();

	private PlayCountDownTimer gameCountDown;
	private ProgressBar gameProgress;

	private SharedPreferences spSettings;
	private ArrayList<String> userCategories = new ArrayList<String>();
	private boolean showCorrectAnswerSetting = true;
	private String gameMode = "standard";
	private TextView questionTextView;
	private ImageView questionImageView;
	private DataBaseHelper dbh;
	private int questionArrayIndex;
	private Button firstOptionButton;
	private Button secondOptionButton;
	private Button thirdOptionButton;
	private Button fourthOptionButton;
	private Button fifthOptionButton;

	OnClickListener optionButtonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {

			if (v == firstOptionButton) {
				addAnsweredQuestion(firstOptionButton.getText().toString());
				buttonPressed(0);
			} else if (v == secondOptionButton) {
				addAnsweredQuestion(secondOptionButton.getText().toString());
				buttonPressed(1);
			} else if (v == thirdOptionButton) {
				addAnsweredQuestion(thirdOptionButton.getText().toString());
				buttonPressed(2);
			} else if (v == fourthOptionButton) {
				addAnsweredQuestion(fourthOptionButton.getText().toString());
				buttonPressed(3);
			} else if (v == fifthOptionButton) {
				addAnsweredQuestion(fifthOptionButton.getText().toString());
				buttonPressed(4);
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);

		android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		actionBar.hide();

		spSettings = getSharedPreferences("com.example.quizapp.SETTINGS",
				Context.MODE_PRIVATE);
		loadPreferences();

		gm = new GameMechanics();

		gameProgress = (ProgressBar) findViewById(R.id.progress_bar);

		questionTextView = (TextView) findViewById(R.id.question_text);
		questionImageView = (ImageView) findViewById(R.id.question_image);

		firstOptionButton = (Button) findViewById(R.id.option1_button);
		firstOptionButton.setOnClickListener(optionButtonListener);

		secondOptionButton = (Button) findViewById(R.id.option2_button);
		secondOptionButton.setOnClickListener(optionButtonListener);

		thirdOptionButton = (Button) findViewById(R.id.option3_button);
		thirdOptionButton.setOnClickListener(optionButtonListener);

		fourthOptionButton = (Button) findViewById(R.id.option4_button);
		fourthOptionButton.setOnClickListener(optionButtonListener);

		fifthOptionButton = (Button) findViewById(R.id.option5_button);
		fifthOptionButton.setOnClickListener(optionButtonListener);

		getNewQuestions();
		printNewQuestion();
	}

	private void loadPreferences() {

		showCorrectAnswerSetting = spSettings.getBoolean("show_answer", true);

		gameMode = spSettings.getString("game_mode", "standard");

		ArrayList<String> categories = new DataBaseHelper(this).getCategories();

		for (int i = 0; i < categories.size(); i++) {
			if (spSettings.getBoolean(categories.get(i), true)) {
				userCategories.add(categories.get(i));
			}
		}
	}

	private void getNewQuestions() {

		dbh = new DataBaseHelper(this);
		if (gm.getAllIds() == null) {
			gm.setAllIds(dbh.getIds(dbh.getCategories()));
		}
		ids = gm.pickIds(gm.getQuestions().length, dbh.getIds(userCategories));

		for (int i = 0; i < gm.getQuestions().length; i++) {
			gm.getQuestions()[i] = new Question(this, ids.get(i));
			gm.removeId(ids.get(i)); 
		}
	}

	private void disableButtons() {
		firstOptionButton.setEnabled(false);
		secondOptionButton.setEnabled(false);
		thirdOptionButton.setEnabled(false);
		fourthOptionButton.setEnabled(false);
		fifthOptionButton.setEnabled(false);
	}


	private void buttonPressed(int button) {

		disableButtons();
		gameCountDown.cancel();
		gm.getPlayer().addPlayTime((gameProgress.getMax()/10) - (gameProgress.getProgress()/10));

		if (button >= 0) {

			if (isCorrectAnswer(button)) {
				gm.getPlayer().incrementQuestionCount();
				gm.getPlayer().addScore((gameProgress.getProgress() / 10) + 5);
//				answer = MediaPlayer.create(GameActivity.this, R.raw.applause);
//				answer.start();
				correctOptionClicked(button);
			} else {
				gm.getPlayer().addScore(0);
				wrongOptionClicked(button);
//				answer = MediaPlayer.create(GameActivity.this, R.raw.boo);
//				answer.start();
				if ("survival".equals(gameMode)) {
					startResultActivity();
					return;
				}
			}
		} else {
			gm.getPlayer().addScore(0);
			if (showCorrectAnswerSetting)
				showCorrectAnswer();
			if ("survival".equals(gameMode)) {
				startResultActivity();
				return;
			}
		}

		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			public void run() {

				if (gm.incrementQuestionNr(gameMode)) {
					startResultActivity();
					return;
				} else {
					if (gm.getQuestionNr() == 0) {
						Log.d("tagg", "Getting new questions");
						getNewQuestions();
					}
					printNewQuestion();
				}
			}
		}, 1500);


	}

	private void correctOptionClicked(int button) {
		markCorrectAnswer(button);

	}

	private void wrongOptionClicked(int button) {

		markWrongAnswer(button);

		if (showCorrectAnswerSetting)
			showCorrectAnswer();
	}

	private void showCorrectAnswer() {
		for (int i = 0; i < 5; i++) {
			if (isCorrectAnswer(i))

				markCorrectAnswer(i);
		}
	}

	private void markCorrectAnswer(int button) {

		switch (button) {
		case 0:
			firstOptionButton
			.setBackgroundResource(R.drawable.right_answer_button);
			break;
		case 1:
			secondOptionButton
			.setBackgroundResource(R.drawable.right_answer_button);
			break;
		case 2:
			thirdOptionButton
			.setBackgroundResource(R.drawable.right_answer_button);
			break;
		case 3:
			fourthOptionButton
			.setBackgroundResource(R.drawable.right_answer_button);
			break;
		case 4:
			fifthOptionButton
			.setBackgroundResource(R.drawable.right_answer_button);
			break;
		}
	}

	private void markWrongAnswer(int button) {

		switch (button) {
		case 0:
			firstOptionButton
			.setBackgroundResource(R.drawable.wrong_answer_button);
			break;
		case 1:
			secondOptionButton
			.setBackgroundResource(R.drawable.wrong_answer_button);
			break;
		case 2:
			thirdOptionButton
			.setBackgroundResource(R.drawable.wrong_answer_button);
			break;
		case 3:
			fourthOptionButton
			.setBackgroundResource(R.drawable.wrong_answer_button);
			break;
		case 4:
			fifthOptionButton
			.setBackgroundResource(R.drawable.wrong_answer_button);
			break;
		}
	}

	private boolean isCorrectAnswer(int button) {
		return gm.checkIfRight((String[]) gm.getQuestions()[gm.getQuestionNr()]
				.getOptions().get(button));
	}

	private void resetButtons() {

		firstOptionButton.setEnabled(true);
		secondOptionButton.setEnabled(true);
		thirdOptionButton.setEnabled(true);
		fourthOptionButton.setEnabled(true);
		fifthOptionButton.setEnabled(true);

		firstOptionButton.setBackgroundResource(R.drawable.standard_button);
		secondOptionButton.setBackgroundResource(R.drawable.standard_button);
		thirdOptionButton.setBackgroundResource(R.drawable.standard_button);
		fourthOptionButton.setBackgroundResource(R.drawable.standard_button);
		fifthOptionButton.setBackgroundResource(R.drawable.standard_button);
	}

	private void printNewQuestion() {

		String[] option1 = (String[]) gm.getQuestions()[gm.getQuestionNr()]
				.getOptions().get(0);
		String[] option2 = (String[]) gm.getQuestions()[gm.getQuestionNr()]
				.getOptions().get(1);
		String[] option3 = (String[]) gm.getQuestions()[gm.getQuestionNr()]
				.getOptions().get(2);
		String[] option4 = (String[]) gm.getQuestions()[gm.getQuestionNr()]
				.getOptions().get(3);
		String[] option5 = (String[]) gm.getQuestions()[gm.getQuestionNr()]
				.getOptions().get(4);

		questionTextView.setText(gm.getQuestions()[gm.getQuestionNr()]
				.getQuestion());

		if (gm.getQuestions()[gm.getQuestionNr()].isImageQuestion()) {
			questionImageView.setImageResource(getResources().getIdentifier(
					gm.getQuestions()[gm.getQuestionNr()].getImageName(),
					"drawable", getPackageName()));
			questionImageView.setVisibility(View.VISIBLE);
		} else {
			questionImageView.setVisibility(View.GONE);
		}

		firstOptionButton.setText(option1[0]);
		secondOptionButton.setText(option2[0]);
		thirdOptionButton.setText(option3[0]);
		fourthOptionButton.setText(option4[0]);
		fifthOptionButton.setText(option5[0]);

		startTimer();
		resetButtons();
	}

	private void startTimer() {

		if (gameCountDown != null) {
			gameCountDown.cancel();
		} else {
			int progress = 150;
			gameProgress.setMax(progress);
			gameProgress.setProgress(progress);
			gameCountDown = new PlayCountDownTimer(progress*100, 100);
		}
		gameProgress.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_green));
		gameCountDown.start();
	}

	private void addAnsweredQuestion(String answer) {
		gm.addAnsweredQuestion(new Question(GameActivity.this, gm.getQuestions()[gm.getQuestionNr()].getQuestionId()));
		questionArrayIndex = gm.getAnsweredQuestions().size()-1;
		gm.getAnsweredQuestions().get(questionArrayIndex).setUserAnswer(answer);
	}

	private void startResultActivity() {

		ArrayList<String> allAnswers = gm.getAllUserAnswers();
		String[] answersArray = allAnswers.toArray(new String[allAnswers.size()]);

		int[] allIds = new int[answersArray.length];

		for (int i = 0; i < gm.getAnsweredQuestions().size(); i++) {
			allIds[i] = gm.getAnsweredQuestions().get(i).getQuestionId();
		}

		Intent intent = new Intent(this, ResultActivity.class);
		intent.putExtra("QUESTIONIDS", allIds);
		intent.putExtra("ANSWERS", answersArray);
		intent.putExtra("SCORE", gm.getPlayer().getScore());
		intent.putExtra("SCORELIST", gm.getPlayer().getScoreList());
		intent.putExtra("PLAYTIME", gm.getPlayer().getPlayTime());
		startActivity(intent);
		finish();
	}

	@Override
	public void onBackPressed() {
		new AlertDialog.Builder(this)
		.setTitle(getString(R.string.really_quit))
		.setMessage(getString(R.string.are_you_sure))
		.setNegativeButton(getString(R.string.cancel), null)
		.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface arg0, int arg1) {
				finish();
			}
		}).create().show();
	}

	@Override
	public void finish() {
		gameCountDown.cancel();
		super.finish();
	}

	private class PlayCountDownTimer extends CountDownTimer {

		private int progress;
		private int max;

		private PlayCountDownTimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			max = (int) millisInFuture;
		}

		@Override
		public void onTick(long millisUntilFinished) {
			if (millisUntilFinished < max * 0.5) {
				if (gameProgress.getProgressDrawable() != getResources().getDrawable(R.drawable.progressbar_red)) {
					gameProgress.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_red));
				}
			}
			progress = (int) (millisUntilFinished / 100);
			gameProgress.setProgress(progress);
		}

		@Override
		public void onFinish() {
			gameProgress.setProgress(0);
			addAnsweredQuestion("TIMEOUT");
			buttonPressed(-1);
		}
	}
}




//do {
//} while (checkForEmptyElements());
//
//
//
//for (int i = 0; i < gm.getQuestions().length; i++) {
//	Log.d("tagg", "ids is " + gm.getQuestions()[i].getQuestion());
//}
//}
//
//public void fillQuestionsArray() {
//
//
//
//for (int i = 0; i < gm.getQuestions().length; i++) {
//
//	if (null == gm.getQuestions()[i]) {
//		for (int j = 0; j < ids.size(); j++) {
//			gm.getQuestions()[i] = new Question(this, ids.get(i));
//		}
//		break;
//	}
//}
//
//for (int i = 0; i > ids.size(); i++) {
//	gm.getQuestions()[i] = new Question(this, ids.get(i));
//
//}
//}
//
//public boolean checkForEmptyElements() {
//
//for (int i = 0; i < gm.getQuestions().length; i++) {
//
//	if (null == gm.getQuestions()[i]) {
//
//		return true;
//	}
//}
//
//return false;
//}


