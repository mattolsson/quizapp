package com.example.quizapp;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;

public class StartActivity extends ActionBarActivity {

	MediaPlayer startSound = new MediaPlayer();

	Intent i = new Intent();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);

		android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		actionBar.hide();

		//startSound = MediaPlayer.create(StartActivity.this, R.raw.holycow);
		//startSound.start();
		Thread timer = new Thread() {
			//Start the activity and runs fore (sleep(millisek)) 
			public void run() {
				try {
					sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					Intent startAct = new Intent(StartActivity.this,
							MainActivity.class);
					startActivity(startAct);
				}
			}
		};
		timer.start();
	}
	//makes the activity  sleep
	public void onPause() {
		super.onPause();
		//startSound.release();
		finish();
	}
}
