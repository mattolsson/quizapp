package com.example.quizapp;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Adapter for highscores list in HighscoresActivity.java
 * @author it-hogskolan
 *
 */
public class HighscoresViewAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private ArrayList<Player> players = new ArrayList<Player>();

	public HighscoresViewAdapter(Activity activity, ArrayList<Player> players) {
		this.players = players;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return players.size();
	}

	@Override
	public Object getItem(int position) {
		return players.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = null;

		if(convertView == null) {
			v = inflater.inflate(R.layout.highscore_layout, parent, false);
		}else {
			v = convertView;
		}

		TextView nameOfPlayer = (TextView)v.findViewById(R.id.player_name_text);
		nameOfPlayer.setText(players.get(position).getName());	

		TextView scoreOfPlayer = (TextView)v.findViewById(R.id.player_score_text);

		scoreOfPlayer.setText(players.get(position).getScore()+"");

		return v;
	}
}
