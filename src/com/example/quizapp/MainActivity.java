package com.example.quizapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * 	Class MainActivity. 
 * 	Buttons for play game, settings, highscores and about. 
 */
public class MainActivity extends ActionBarActivity {
	public MediaPlayer mediaPlayer;


	private OnClickListener playGameButtonListener = new OnClickListener() {
		public void onClick(View v) {
			playGameButtonPressed();
		}
	};

	private OnClickListener settingsButtonListener = new OnClickListener() {
		public void onClick(View v) {
			settingsButtonPressed();
		}
	};

	private OnClickListener highscoreButtonListener = new OnClickListener() {
		public void onClick(View v) {
			highscoreButtonPressed();
		}
	};

	private OnClickListener aboutGameButtonPressed = new OnClickListener(){

		public void onClick(View v) {
			aboutGameButtonPressed();
		}

	};


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		actionBar.hide();

		DataBaseHelper dataBase = new DataBaseHelper(this);
		dataBase.getCategories();

		//        SharedPreferences spCategories = getSharedPreferences("CATEGORIES", Context.MODE_PRIVATE);

		Button playGameButton = (Button) findViewById(R.id.play_button);
		playGameButton.setOnClickListener(playGameButtonListener);

		Button settingsButton = (Button) findViewById(R.id.settings_button);
		settingsButton.setOnClickListener(settingsButtonListener);

		Button highscoreButton = (Button) findViewById(R.id.highscore_button);
		highscoreButton.setOnClickListener(highscoreButtonListener);

		Button aboutGameButton = (Button) findViewById(R.id.about_button);
		aboutGameButton.setOnClickListener(aboutGameButtonPressed);
	}

	private void playGameButtonPressed() {
		Intent intent = new Intent(this, GameActivity.class);
		startActivity(intent);

		//    	MediaPlayer startSound = MediaPlayer.create(MainActivity.this, R.raw.squeal3) ;
		//		startSound.start();
	}

	private void settingsButtonPressed() {
		Intent intent = new Intent(this, SettingsActivity.class);
		startActivity(intent);

		//    	MediaPlayer startSound = MediaPlayer.create(MainActivity.this, R.raw.wreee);
		//
		//    	startSound.start();
	}

	private void highscoreButtonPressed() {
		Intent intent = new Intent(this, HighscoreActivity.class);
		startActivity(intent);

		//		MediaPlayer startSound = MediaPlayer.create(MainActivity.this, R.raw.wreee);
		//
		//		startSound.start();

	}

	private void aboutGameButtonPressed(){
		Intent aboutIntent = new Intent(this,AboutGame.class);
		startActivity(aboutIntent);

		//		MediaPlayer startSound = MediaPlayer.create(MainActivity.this, R.raw.wreee);
		//
		//		startSound.start();
	}
}
