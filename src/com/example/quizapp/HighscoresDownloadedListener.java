package com.example.quizapp;

import org.json.JSONArray;

public interface HighscoresDownloadedListener {

	public void HighscoresDownloaded(JSONArray result);

}
