package com.example.quizapp;

import java.util.ArrayList;
import java.util.Collections;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;

/**
 * Class representing a question
 * @author it-hogskolan
 *
 */
public class Question extends ActionBarActivity{

	private String question;
	private int questionId;
	private ArrayList<String[]> options = new ArrayList<String[]>();
	private String category;
	private String userAnswer;
	private String rightAnswer;
	private String imageName;
	private boolean imageQuestion;

	public Question (Context context, int questionId) {
		fillArrayList(new DataBaseHelper(context).readQuestion(questionId));
		shuffleArrayList();
		checkForImage();
	}

	private void fillArrayList(String[] strings) {
		for (int i = 0; i < strings.length; i++) {
			if (i == 0) {
				questionId = Integer.parseInt(strings[i]);
			} else if (i == 1) {
				category = strings[i];
			} else if (i == 2){
				question = strings[i];
			} else if (i == 3) {
				String[] temp2 = {strings[i], "right"};
				options.add(temp2);
				rightAnswer = strings[i];
			} else {
				String[] temp3 = {strings[i], "wrong"};
				options.add(temp3);
			}
		}
	}

	private void shuffleArrayList() {
		Collections.shuffle(options);		
	}

	private void checkForImage() {
		if (question.contains("§")) {
			String[] s = question.split("§");
			question = s[0];
			imageName = s[1].replace(" ", "");
			imageQuestion = true;
		} else {
			imageQuestion = false;
		}
	}

	public String getRightAnswer() {
		return rightAnswer;
	}

	public String getQuestion() {
		return question;
	}
	public ArrayList<String[]> getOptions() {
		return options;
	}

	public void setUserAnswer(String answer) {
		userAnswer = answer;
	}

	public String getUserAnswer() {
		return userAnswer;
	}

	public int getQuestionId() {
		return questionId;
	}

	public String getCategory() {
		return category;
	}

	public boolean isImageQuestion() {
		return imageQuestion;
	}

	public String getImageName() {
		return imageName;
	}
}
