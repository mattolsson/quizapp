package com.example.quizapp;

import java.util.ArrayList;

/**
 * Class representing a player
 * @author it-hogskolan
 *
 */
public class Player {

	private int score, playTime, questionCount;
	private ArrayList<Integer> scoreList;
	private String name;

	public Player() {
		scoreList = new ArrayList<Integer>();
		resetScore();
	}

	public int getScore() {
		return score;
	}

	public void addScore(int add) {
		score += add;
		scoreList.add(add);
	}

	public void resetScore() {
		score=0;
		scoreList.clear();
		playTime=0;
	}

	public ArrayList<Integer> getScoreList() {
		return scoreList;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPlayTime() {
		return playTime;
	}

	public void addPlayTime(int addedTime) {
		playTime += addedTime;
	}

	public void incrementQuestionCount() {
		questionCount++;
	}

	public int getQuestionCount() {
		return questionCount;
	}

}
