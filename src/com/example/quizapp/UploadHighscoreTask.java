package com.example.quizapp;

import org.json.JSONArray;

import android.os.AsyncTask;

/**
 * Class to handle uploading of highscore to remote MySQL Database
 * @author it-hogskolan
 *
 */
public class UploadHighscoreTask extends AsyncTask<Connector, Long, JSONArray> {

	private HighscoreUploadedListener listener;
	private Player player;

	public UploadHighscoreTask(HighscoreUploadedListener listener, Player player) {
		this.listener = listener;
		this.player = player;
	}

	@Override
	protected JSONArray doInBackground(Connector... params) {
		return params[0].uploadHighscore(player);
	}

	@Override
	protected void onPostExecute(JSONArray result) {
		listener.HighscoreUploaded(result);
		super.onPostExecute(result);
	}
}
