package com.example.quizapp;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.RadioGroup;

public class SettingsActivity extends ActionBarActivity {


	private CheckedTextView musicCheckBox; 
	private CheckedTextView showAnswerCheckBox;
	private SharedPreferences sp;
	private ArrayList<String> categories;
	private ArrayList<Integer> ids;
	private ListView lv;
	private SettingsViewAdapter svAdapter;
	private Button backButton;

	MediaPlayer mPlayer = new MediaPlayer();

	private SharedPreferences.Editor editor;
	private RadioGroup gameModeGroup;


	OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			showAnswerCheckBox.toggle();
		}

	};

	OnClickListener musicListener = new OnClickListener() {
		public void onClick(View v) {
//			musicCheckBox.toggle();
//			musicCheckBox();
		}

	};

	/*	 
	OnClickListener soundListener = new OnClickListener() { 

	    @Override
	    public void onClick(View v) {
		    buttonSoundCheckbox.toggle();
	}
};
	 */

	private OnClickListener playListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			backToMainPressed();
		}
	};


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);


		android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		actionBar.hide();

		sp = getSharedPreferences("com.example.quizapp.SETTINGS", Context.MODE_PRIVATE);

		categories = new DataBaseHelper(this).getCategories();

		svAdapter = new SettingsViewAdapter(this, categories, sp);
		lv = (ListView) findViewById(R.id.categories_list);
		lv.setAdapter(svAdapter);

		ids = svAdapter.getIds();

		showAnswerCheckBox = (CheckedTextView) findViewById(R.id.show_answer_checkBox);


		showAnswerCheckBox.setOnClickListener(clickListener);	

		musicCheckBox = ( CheckedTextView) findViewById(R.id.music_button_checkbox);		 
		musicCheckBox.setOnClickListener(musicListener);	



		//		CheckedTextView backgroundMusicCheckbox = (CheckedTextView) findViewById(R.id.toggle_music_button);
		//		backgroundMusicCheckbox.setOnClickListener(musicListener);
		//		
		//		sounds = (CheckedTextView) findViewById(R.id.toggle_sound_button);
		//		sounds.setOnClickListener(soundListener);
		//
		//		backgroundMusicCheckbox = (CheckedTextView) findViewById(R.id.music_button_checkbox);
		//		backgroundMusicCheckbox.setOnClickListener(musicListener);
		//		
		//		buttonSoundCheckbox = (CheckedTextView) findViewById(R.id.sound_button_checkbox);
		//		buttonSoundCheckbox.setOnClickListener(soundListener);

		 

		backButton = (Button) findViewById(R.id.back_button);
		backButton.setOnClickListener(playListener);


		sp = getSharedPreferences("com.example.quizapp.SETTINGS", Context.MODE_PRIVATE);
		gameModeGroup = (RadioGroup) findViewById(R.id.game_mode_radio_grup);



		loadPreferences();
	}

//	private void  musicCheckBox() {
//
//
//		if (musicCheckBox.isChecked()) {
//			mPlayer = MediaPlayer.create(SettingsActivity.this, R.raw. shadow);
//			mPlayer.setLooping(true);
//			mPlayer.start();
//
//		} else {
//			mPlayer.stop();	
//		}		 
//	}

	private void backToMainPressed() {
//		MediaPlayer buttonSound2 = MediaPlayer.create(SettingsActivity.this, R.raw. wreee);
//		buttonSound2.start();

		onBackPressed();
	};

	private void loadPreferences() {
		showAnswerCheckBox.setChecked(sp.getBoolean("show_answer", true));
		gameModeGroup.check(sp.getInt("radioId", R.id.standard_mode_radio));
	}
	private void savePreferences() {

		editor = sp.edit();
		editor.putBoolean("show_answer", showAnswerCheckBox.isChecked());

		int index = 0;

		for (int i = 0; i < ids.size(); i++) {
			View v = findViewById(ids.get(i));
			if (v != null) {
				CheckedTextView checkBox = (CheckedTextView) v.findViewById(R.id.categories_checkbox);
				editor.putBoolean(categories.get(index), checkBox.isChecked());
				index++;
			}
		}
		editor.commit();

		SharedPreferences.Editor editor2 = sp.edit();
		int radioId = gameModeGroup.getCheckedRadioButtonId();

		if (radioId == R.id.standard_mode_radio) {
			editor2.putString("game_mode", "standard");
			editor2.putInt("radioId", R.id.standard_mode_radio);
			editor2.commit();
		} else if (radioId == R.id.survival_mode_button) {
			editor2.putString("game_mode", "survival");
			editor2.putInt("radioId", R.id.survival_mode_button);
			editor2.commit();
		}

	}



	private boolean categorySelected() {

		for (int i = 0; i < ids.size(); i++) {
			View v = findViewById(ids.get(i));
			if (v != null) {
				CheckedTextView checkBox = (CheckedTextView) v.findViewById(R.id.categories_checkbox);
				if (checkBox.isChecked()) {
					return true;
				}
			}
		}
		return false;
	}

	private void showAlertDialog() {
		new AlertDialog.Builder(this)
		.setTitle(getString(R.string.no_category_selected))
		.setMessage(getString(R.string.choose_a_category))
		.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface arg0, int arg1) {

			}
		}).create().show();
	}

	@Override
	public void onBackPressed() {
		if (!categorySelected()) {
			showAlertDialog();
		} else {
			savePreferences();
			super.onBackPressed();
		}
	}
}
