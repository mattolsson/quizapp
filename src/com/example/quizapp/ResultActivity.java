package com.example.quizapp;

import java.util.ArrayList;

import org.json.JSONArray;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
/**
 *	Class ResultActivity. 
 *	Shows the result from last game played. Shows questions, right answer, users answer and score. User can put score into highscore list.   
 */
public class ResultActivity extends ActionBarActivity implements HighscoreUploadedListener{

	private int[] ids;
	private String[] answers;
	private int score, playTime;
	private ArrayList<Question> showResult;
	private Player player;
	private DataBaseHelper dbh;
	private SharedPreferences spSettings;
	private boolean showCorrectAnswerSetting;
	private ArrayList<Integer> scoreList;

	private AlertDialog.Builder alert;
	MediaPlayer buttonSound1 = new MediaPlayer();
	private OnClickListener playGameButtonListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			playGameButtonPressed();
		}
	};

	private OnClickListener menyButtonListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			menyButtonPressed();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_result);

		android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		actionBar.hide();

		Intent intent = getIntent();
		ids = intent.getIntArrayExtra("QUESTIONIDS");
		answers = intent.getStringArrayExtra("ANSWERS");

		score = intent.getIntExtra("SCORE", 0);
		scoreList = new ArrayList<Integer>(intent.getIntegerArrayListExtra("SCORELIST"));
		playTime = intent.getIntExtra("PLAYTIME", 0);

		showResult = new ArrayList<Question>();
		dbh = new DataBaseHelper(this);
		player = new Player();
		player.setScore(score);

		spSettings = getSharedPreferences("com.example.quizapp.SETTINGS",
				Context.MODE_PRIVATE);
		showCorrectAnswerSetting = spSettings.getBoolean("show_answer", true);

		if (score>0) {
			startAlertDialog();
		}

		for (int i = 0; i < ids.length; i++) {
			showResult.add(new Question(this, ids[i]));
		}

		ResultViewAdapter adapter = new ResultViewAdapter(this, showResult, answers, showCorrectAnswerSetting, scoreList);
		ListView lv = (ListView)findViewById(R.id.result_list);
		lv.setAdapter(adapter);

		TextView tv = (TextView)findViewById(R.id.score_text);
		tv.setText("Du skrapade ihop "+score+" poäng på totalt "+playTime+" sekunder!");

		Button playGameButton = (Button) findViewById(R.id.play_button);
		playGameButton.setOnClickListener(playGameButtonListener);

		Button menyButton = (Button) findViewById(R.id.main_button);
		menyButton.setOnClickListener(menyButtonListener);

	}

	private void startAlertDialog() {
		alert = new AlertDialog.Builder(this);
		alert.setTitle(R.string.highscore_message);
		alert.setMessage(R.string.enter_name);

		final EditText input = new EditText(this);
		input.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		input.setSingleLine(true);
		input.setFilters(new InputFilter[] { new InputFilter.LengthFilter(15) });
		alert.setView(input);

		alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				player.setName(input.getText().toString());
				dbh.addHighscore(player.getName(), player.getScore());

				UploadHighscoreTask uploadHighscore = new UploadHighscoreTask(ResultActivity.this, player);
				uploadHighscore.execute(new Connector());
			}
		});

		alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub

			}

		});

		AlertDialog dialog = alert.create();
		dialog.show();
		final Button okButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
		okButton.setEnabled(false);
		input.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(input.getText().toString().length() != 0) {
					okButton.setEnabled(true);
				}else {
					okButton.setEnabled(false);
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}

			@Override
			public void afterTextChanged(Editable s) {}
		});

	}

	private void menyButtonPressed() {
		//		buttonSound1 = MediaPlayer.create( this, R.raw. wreee);
		//    	buttonSound1.start();
		finish();
	}

	private void playGameButtonPressed() {
		Intent intent = new Intent(this, GameActivity.class);
		startActivity(intent); 
		//    	buttonSound1 = MediaPlayer.create( ResultActivity.this, R.raw. wreee);
		//    	buttonSound1.start();

		finish();
	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	public void HighscoreUploaded(JSONArray result) {

	}

}
