package com.example.quizapp;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
/**
 *	Class ResultViewAdapter
 *	Adapter to make ListView in ResultActivity work. 
 */
public class ResultViewAdapter extends BaseAdapter {

	private ArrayList<Question> adapterData;
	private LayoutInflater inflater;
	private String[] answers;
	private boolean showCorrectAnswerSetting;
	private ArrayList<Integer> scoreList;

	/**
	 * Constructor ResultViewAdapter
	 * Takes arguments needed to set adapter to ListView.
	 * @param activity, takes info from ResultActivity.  
	 * @param question, ArrayList with questions from last game played.
	 * @param answers, String[] with answers from last game played.
	 * @param setting, boolean from settings to show if user wants "show correct answers".  
	 * @param scoreList, ArrayList with scores from last game. 
	 */
	public ResultViewAdapter(Activity activity, ArrayList<Question> question, String[] answers, boolean setting, ArrayList<Integer> scoreList) {
		adapterData = question;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.answers = answers;
		showCorrectAnswerSetting = setting;
		this.scoreList = new ArrayList<Integer>(scoreList);
	}

	@Override
	public int getCount() {
		return adapterData.size();
	}

	@Override
	public Object getItem(int position) {
		return adapterData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}


	@SuppressLint("ViewHolder")@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = null;

		//		if(convertView == null) {
		v = inflater.inflate(R.layout.result_layout, parent, false);
		//		}else {
		//			v = convertView;
		//			Log.d("tagg", "convertview lyckades");
		//		}

		TextView questionText = (TextView)v.findViewById(R.id.question_text);
		questionText.setText(adapterData.get(position).getQuestion());

		TextView userAnswer = (TextView)v.findViewById(R.id.user_answer_text);

		TextView correctAnswer = (TextView)v.findViewById(R.id.correct_answer_text);
		correctAnswer.setText("Rätt svar: "+adapterData.get(position).getRightAnswer());

		if (!showCorrectAnswerSetting) {
			correctAnswer.setVisibility(View.GONE);
		}

		if (answers[position].equals(adapterData.get(position).getRightAnswer())) {
			userAnswer.setText("Ditt svar: "+answers[position]+" - RÄTT! ("+scoreList.get(position)+" poäng)");
			userAnswer.setTextColor(Color.GREEN);
			correctAnswer.setVisibility(View.GONE);
		} else if (answers[position].equals("TIMEOUT")) {
			userAnswer.setText("Ditt svar: (Inget, tiden tog slut!)");

		} else {
			userAnswer.setText("Ditt svar: "+answers[position]+" - FEL!");
			userAnswer.setTextColor(Color.RED);
		}

		if (!showCorrectAnswerSetting) {
			correctAnswer.setVisibility(View.GONE);
		}

		return v;
	}
}
