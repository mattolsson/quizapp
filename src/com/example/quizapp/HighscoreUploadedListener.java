package com.example.quizapp;

import org.json.JSONArray;

public interface HighscoreUploadedListener {
	
	public void HighscoreUploaded(JSONArray result);
	
}
