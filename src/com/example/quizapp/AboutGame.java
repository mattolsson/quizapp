package com.example.quizapp;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * This class is an activity for informations about us and game rules
 * @author Group 1
 *
 */
public class AboutGame extends ActionBarActivity {

	Button backToMain;

	private OnClickListener aboutListener= new OnClickListener(){

		@Override
		public void onClick(View v) {
			onBackPressed();
		}

	};

	@Override
	public void onBackPressed(){
		//		MediaPlayer buttonSound2 = MediaPlayer.create(AboutGame.this, R.raw. wreee);
		//    	buttonSound2.start();

		finish();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_game);

		android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		actionBar.hide();

		backToMain = (Button)findViewById(R.id.back_to_main);
		backToMain.setOnClickListener(aboutListener);
	}

}
