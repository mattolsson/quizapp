package com.example.quizapp;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

/**
 * Adapter for categories list in SettingsActivity.java
 * @author it-hogskolan
 *
 */
public class SettingsViewAdapter extends BaseAdapter {

	private ArrayList<String> categories;
	private LayoutInflater inflater;
	private int checkBoxId = 1;
	private static ArrayList<Integer> ids = new ArrayList<Integer>();
	private SharedPreferences sp;

	OnClickListener categoryCheckBoxListener = new View.OnClickListener() {
		public void onClick(View v) {
			((CheckedTextView) v).toggle();
		}
	};

	public SettingsViewAdapter (Activity activity, ArrayList<String> categories, SharedPreferences sp) {
		this.categories = categories;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.sp = sp;
		ids.clear();
	}

	@Override
	public int getCount() {
		return categories.size();
	}

	@Override
	public String getItem(int position) {
		return categories.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = null;

		if(convertView == null) {
			v = inflater.inflate(R.layout.settings_layout, parent, false);
		}else {
			v = convertView;
		}

		CheckedTextView categoryCheckbox = (CheckedTextView) v.findViewById(R.id.categories_checkbox);
		categoryCheckbox.setText(categories.get(position));
		categoryCheckbox.setChecked(sp.getBoolean(categories.get(position), true));
		categoryCheckbox.setOnClickListener(categoryCheckBoxListener);

		v.setId(checkBoxId);
		ids.add(v.getId());

		checkBoxId++;

		return v;
	}


	public ArrayList<Integer> getIds() {
		return ids;
	}
}
