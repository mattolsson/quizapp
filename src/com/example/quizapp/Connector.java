package com.example.quizapp;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

/**
 * Class to handle connection to remote MySQL database
 * @author it-hogskolan
 *
 */
public class Connector {

	/**
	 * Downloads all highscores from remote MySQL database
	 * @return JSONArray
	 */
	public JSONArray downloadHighscores() {

		HttpParams httpParameters = new BasicHttpParams();

		HttpConnectionParams.setConnectionTimeout(httpParameters, 10000);
		HttpConnectionParams.setSoTimeout(httpParameters, 10000);

		HttpEntity httpEntity = null;

		try {

			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet("http://www.landstinget-band.se/quiz/gethighscores.php");
			HttpResponse httpResponse = httpClient.execute(httpGet);
			httpEntity = httpResponse.getEntity();
		} catch (Exception e) {

		}
		return convertToJSONArray(httpEntity);
	}

	/**
	 * Uploads highscore to remote MySQL database
	 * @param player - Player, player instance containing the name and score to upload
	 * @return
	 */
	public JSONArray uploadHighscore(Player player) {

		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost("http://www.landstinget-band.se/quiz/addhighscore.php");

		try {
			List<BasicNameValuePair> valuePair = new ArrayList<BasicNameValuePair>();
			valuePair.add(new BasicNameValuePair("name", player.getName()));
			valuePair.add(new BasicNameValuePair("score", player.getScore()+""));
			httpPost.setEntity(new UrlEncodedFormEntity(valuePair));
			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			String htmlResponse = EntityUtils.toString(entity);
			Log.d("tagg", htmlResponse);
		} catch (UnsupportedEncodingException e) {
			Log.d("tagg", "Exception 1");
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			Log.d("tagg", "Exception 2");
			e.printStackTrace();
		} catch (IOException e) {
			Log.d("tagg", "Exception 3");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Converts HttpEntity to JSONArray
	 * @param httpEntity HttpEntity to convert
	 * @return JSONArray
	 */
	private JSONArray convertToJSONArray(HttpEntity httpEntity) {
		JSONArray jsonArray = null;

		if (httpEntity != null) {
			try {
				String entityResponse = EntityUtils.toString(httpEntity);
				jsonArray = new JSONArray(entityResponse);
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return jsonArray;
	}
}
