package com.example.quizapp;

import org.json.JSONArray;

import android.os.AsyncTask;

/**
 * AsyncTask to handle download of highscores
 * @author it-hogskolan
 *
 */
public class DownLoadHighscoresTask extends AsyncTask<Connector, Long, JSONArray> {

	private HighscoresDownloadedListener listener;

	public DownLoadHighscoresTask(HighscoresDownloadedListener listener) {
		this.listener = listener;
	}

	@Override
	protected JSONArray doInBackground(Connector... params) {
		return params[0].downloadHighscores();
	}

	@Override
	protected void onPostExecute(JSONArray result) {
		listener.HighscoresDownloaded(result);
		super.onPostExecute(result);
	}
}
