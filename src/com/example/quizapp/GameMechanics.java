package com.example.quizapp;

import java.util.ArrayList;
import java.util.Random;

/**
 * Class to handle game mechanics
 * @author it-hogskolan
 *
 */
public class GameMechanics {

	private Player player = new Player();
	private Question[] questions = new Question[10];
	private int questionNr;
	private ArrayList<Question> answeredQuestions = new ArrayList<Question>();
	private ArrayList<Integer> allIds = new ArrayList<Integer>();

	/**
	 * Checks if the answer given is correct
	 * @param options - String[] of the options
	 * @return True if correct answer
	 */
	public boolean checkIfRight(String[] options) {
		return "right".equals(options[1]);
	}

	/**
	 * Checks if last question in Question[] and increments questionNr if not
	 * @param gameMode - String - Standardmode or survivalmode
	 * @return True if questionNr was incremented
	 */
	public boolean incrementQuestionNr(String gameMode) {
		questionNr++;
		if (getQuestionNr() == getQuestions().length) {
			resetQuestionNr();
			return "standard".equals(gameMode);
		} else {
			return false;
		}
	}

	/**
	 * Randomly picks question IDs. Removes the picked ones to make sure same question dosn't appear twice
	 * @param howMany - Int, how many IDs to get
	 * @param ids - ArrayList<Integer>, the IDs to pick between
	 * @return ArrayList<Integer>, the IDs that got picked
	 */
	public ArrayList<Integer> pickIds(int howMany, ArrayList<Integer> ids) {
		ArrayList<Integer> randoms = new ArrayList<Integer>();

		Random random = new Random();
		try {
			for (int i = 0; i < howMany; i++) {
				int randomInt = random.nextInt(ids.size());
				randoms.add(ids.get(randomInt));
				ids.remove(randomInt);
			}
		} catch (IllegalArgumentException e) {
			return randoms;
		}
		return randoms;
	}

	/**
	 * Gets all the answers the player has given during the round
	 * @return ArrayList<String> All user answers from the round
	 */
	public ArrayList<String> getAllUserAnswers() {
		ArrayList<String> allUserAnswers = new ArrayList<String>();
		for (int i = 0; i < answeredQuestions.size(); i ++) {
			allUserAnswers.add(answeredQuestions.get(i).getUserAnswer());
		}
		return allUserAnswers;
	}

	/**
	 * Removes ID of question in allIds
	 * @param idToRemove Int, id to remove
	 */
	public void removeId(int idToRemove) {

		for (int i = 0; i < allIds.size(); i++) {
			if (idToRemove == allIds.get(i)) {
				allIds.remove(i);
			}
		}
	}

	public Player getPlayer() {
		return player;
	}

	public Question[] getQuestions() {
		return questions;
	}

	public int getQuestionNr() {
		return questionNr;
	}

	public void resetQuestionNr() {
		questionNr = 0;
	}

	public void addAnsweredQuestion(Question question) {
		answeredQuestions.add(question);
	}

	public ArrayList<Question> getAnsweredQuestions() {
		return answeredQuestions;
	}

	public void setAllIds(ArrayList<Integer> allIds) {
		this.allIds = allIds;
	}

	public ArrayList<Integer> getAllIds() {
		return allIds;
	}
}
