package com.example.quizapp;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class HighscoreActivity extends ActionBarActivity implements HighscoresDownloadedListener {

	private ArrayList<Player> local;
	private ArrayList<Player> global = new ArrayList<Player>();

	private ListView globalScoresList;
	private ListView localScoresList;

	private JSONArray globalScores;
	private RadioGroup switchScoresList;

	private int[] visibilities = {View.VISIBLE, View.GONE};

	private OnCheckedChangeListener switchScoresListListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {

			try {
				int temp = visibilities[0];
				visibilities[0] = visibilities[1];
				visibilities[1] = temp;

				localScoresList.setVisibility(visibilities[0]);
				globalScoresList.setVisibility(visibilities[1]);
			} catch (Exception e) {
				//If list havn't loaded it throws nullPointerException
			}

		}
	};

	private OnClickListener backButtonListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			onBackPressed();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_highscore);

		DownLoadHighscoresTask downLoadHighscores = new DownLoadHighscoresTask(this);
		downLoadHighscores.execute(new Connector());

		android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		actionBar.hide();

		switchScoresList = (RadioGroup) findViewById(R.id.switch_scores_list_radio_grup);
		switchScoresList.setOnCheckedChangeListener(switchScoresListListener);

		Button backButton = (Button) findViewById(R.id.back_from_highscores_button);
		backButton.setOnClickListener(backButtonListener);

		local = new DataBaseHelper(this).getHighscores();

		sortHighscores();

		HighscoresViewAdapter adapter = new HighscoresViewAdapter(this, local);
		localScoresList = (ListView)findViewById(R.id.local_scores_list);
		localScoresList.setAdapter(adapter);
	}

	private void convertJSONtoPlayer() {
		Player player;
		if (globalScores != null) {
			for (int i = 0; i < globalScores.length(); i++) {
				JSONObject json = null;
				try {
					player = new Player();
					json = globalScores.getJSONObject(i);
					player.setScore(json.getInt("score"));
					player.setName(json.getString("name"));
					global.add(player);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void sortHighscores() {
		boolean swapped = true;
		int counter = 1;
		while (swapped) {
			swapped = false;
			for (int i= 0; i < local.size() - counter; i++) {
				if(local.get(i).getScore() < local.get(i + 1).getScore()) {
					Player temp = local.get(i);
					local.set(i, local.get(i + 1));
					local.set(i + 1, temp);
					swapped = true;
				}
			}
		}
	}

	private void setGlobalScores(JSONArray globalScores) {
		this.globalScores = globalScores;
	}

	@Override
	public void onBackPressed() {
		//		MediaPlayer buttonSound = MediaPlayer.create(HighscoreActivity.this, R.raw.wreee );
		//    	buttonSound.start();
		finish();
	}

	@Override
	public void HighscoresDownloaded(JSONArray result) {

		setGlobalScores(result);

		convertJSONtoPlayer();

		HighscoresViewAdapter adapter = new HighscoresViewAdapter(this, global);
		globalScoresList = (ListView)findViewById(R.id.global_scores_list);
		globalScoresList.setAdapter(adapter);

	}
}

